use core::time::Duration;
use std::fs;
use std::thread;
use clap::{Arg,App};
const BAUD: u32 = 9_600;

fn main() {
  let args = App::new("hp7475a-serial")
    .version("0.1.0")
    .about("Serial interface for HP7475A")
    .author("Ryan Metzler")
    .args(&[
      Arg::new("port")
        .about("Serial Port")
        .short('p')
        .long("port")
        .takes_value(true),
      Arg::new("file")
        .about("File to draw")
        .short('f')
        .long("file")
        .takes_value(true),
    ]).get_matches();

  let serial_port = args.value_of("port").unwrap();
  let hpgl_file = args.value_of("file").unwrap();
  let hpgl_data: String = get_file(&hpgl_file);

  let mut port = serialport::new(&serial_port.to_string(), BAUD)
    .timeout(Duration::from_secs(10))
    .open()
    .expect("Failed to open port");

  let lines = hpgl_data.split(";");
  for row in lines {
    if row.len() == 0 { continue }

    let line = row.to_owned() + ";";
    write_when_ready(&mut *port, &line);
  }
}

fn get_file(filename: &str) -> String {
  let contents = fs::read_to_string(filename)
    .expect("Something went wrong reading file");
  return contents;
}

fn write_when_ready(port: &mut dyn serialport::SerialPort, line: &String) {
  while !has_buffer_available(&mut *port, &line) {
    thread::sleep(Duration::from_secs(1));
  };
  match port.write(&line.as_bytes()) {
    Ok(_) => {
      thread::sleep(Duration::from_millis(50));
    }
    _ => {
      panic!("Unable to write data");
    }
  }
}

fn has_buffer_available(port: &mut dyn serialport::SerialPort, line: &String) -> bool {
  let read_command = Vec::from([0x1Bu8, 0x2Eu8, 0x42u8, 0x0Du8]);
  match port.write(&read_command) {
    Ok(_) => {
      let mut read_data = Vec::new();
      loop {
        // Read one byte at a time until we recieve a line feed
        let mut serial_buf: Vec<u8> = vec![0; 1];
        port.read(serial_buf.as_mut_slice()).expect("Unable to read byte from port");

        if serial_buf[0] == 0x0Du8 { break }

        read_data.append(&mut serial_buf);
      }

      let result = String::from_utf8(read_data).unwrap();
      let available_buffer: u32 = result.parse().expect("not a number?");

      return available_buffer as usize > (line.len() + 6)
    }
    _ => {
      panic!("Unable to get buffer status data");
    }
  }

  // read one char at a time

}