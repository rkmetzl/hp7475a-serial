# hp7475a-serial
## A Rust app for writing hpgl data to the HP7475a pen plotter

### Build instructions

Building hp7475a-serial is very simple using `cargo`:
```bash
cargo build
# If you want to install the created binary:
cargo install
```

This has been built and tested on OS X only, but is written to build on any POSIX system.

### Usage

First, ensure the plotter has the following serial configuration:
- 9600 baud
- Zero stop bits
- Y/D switch set to 'D'

(More details on serial configuration can be found [here](https://phlebowtish.wordpress.com/2013/02/22/a-guide-to-setting-up-a-hp7475a-on-a-modern-machine/))

hp7475a-serial is very easy to run. It takes two arguments:

`-f, --file`: The path to the hpgl file to be written

`-p, --port`: The port to write to

Example:
```bash
hp7475a-serial -f path/to/file.hpgl -p /dev/tty-port
```
